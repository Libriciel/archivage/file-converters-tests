<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('ROOT')) {
    define('ROOT', dirname(__DIR__));
}
if (!defined('TESTS')) {
    define('TESTS', ROOT.DS.'tests');
}

require ROOT.DS.'vendor'.DS.'autoload.php';
